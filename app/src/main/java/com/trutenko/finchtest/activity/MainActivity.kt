package com.trutenko.finchtest.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.trutenko.finchtest.R
import com.trutenko.finchtest.adapter.UsersAdapter
import com.trutenko.finchtest.data.User
import com.trutenko.finchtest.presenter.UsersListPresenter
import com.trutenko.finchtest.view.UsersListView
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.direct
import org.kodein.di.generic.instance
import org.kodein.di.newInstance

class MainActivity : MvpAppCompatActivity(), UsersListView, KodeinAware {

    override val kodein: Kodein by closestKodein()

    @InjectPresenter
    lateinit var presenter: UsersListPresenter

    @ProvidePresenter
    fun providePresenter(): UsersListPresenter {
        return kodein.direct.newInstance {
            UsersListPresenter(instance())
        }
    }

    lateinit var usersListView: RecyclerView
    lateinit var loadingIndicator: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        usersListView = findViewById(R.id.users_list)
        loadingIndicator = findViewById(R.id.loading_indicator)

        usersListView.setHasFixedSize(true)
        usersListView.layoutManager = LinearLayoutManager(this)
        usersListView.adapter = UsersAdapter(this) {
            startActivity(UserActivity.newIntent(this,
                    it.avatarUrl,
                    it.nickname,
                    it.reposAmount,
                    it.directLink
                )
            )
        }

    }

    override fun growUserList(users: List<User>) {
        val adapter = usersListView.adapter as UsersAdapter
        adapter.users.addAll(users)
        adapter.notifyDataSetChanged()
    }

    override fun startLoading() {
        loadingIndicator.visibility = View.VISIBLE
    }

    override fun stopLoading() {
        loadingIndicator.visibility = View.INVISIBLE
    }

    override fun showLoadingError() {
        Toast.makeText(this, R.string.connection_error, Toast.LENGTH_LONG).show()
    }

    override fun hideLoadingError() {} // never called

}
